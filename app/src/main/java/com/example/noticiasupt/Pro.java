package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Pro extends AppCompatActivity {

    ArrayList<String> CatalogoProcesadores = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();
    ArrayList<String> CatalogoNucleos = new ArrayList<>();
    ArrayList<String> CatalogoFrecuencia = new ArrayList<>();
    public ListView lvlistapro;
    public TextView tvid, tvnombre, tvnucleos, tvfrecuencia;
    public String cid [];
    public String nucleos [];
    public String frecuencia [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro);

        tvid = (TextView) findViewById(R.id.tvid);
        tvnombre = (TextView) findViewById(R.id.tvnombre);
        tvnucleos = (TextView) findViewById(R.id.tvnucleo);
        tvfrecuencia = (TextView) findViewById(R.id.tvfrecuencia);
        lvlistapro = (ListView) findViewById(R.id.lvprocesadores);

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.list_item_disenolu,CatalogoProcesadores);

        lvlistapro.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvnombre.setText("Nombre: " +lvlistapro.getItemAtPosition(position));
                tvid.setText("Id: "+ cid[position]);
                tvnucleos.setText("Nucleo"+ nucleos[position]);
                tvfrecuencia.setText("Frecuencia"+ frecuencia[position]);

            }
        });



        ServicioPeticion service = Api.getApi(Pro.this).create(ServicioPeticion.class);
        Call<Peticion_Procesador> iniciarCall = service.get_Procesadores();
        iniciarCall.enqueue(new Callback<Peticion_Procesador>() {
            @Override
            public void onResponse(Call<Peticion_Procesador> call, Response<Peticion_Procesador> response) {
                Peticion_Procesador peticion = response.body();

                if (peticion.estado.equals("true")){

                    List<Clase_Procesador> procesadores = peticion.procesadores;

                    for (Clase_Procesador mostrar : procesadores){
                        CatalogoProcesadores.add(mostrar.getNombre());
                    }
                    lvlistapro.setAdapter(arrayAdapter);

                    for (Clase_Procesador mostrarid : procesadores){
                        CatalogoId.add(mostrarid.getId());
                    }
                    cid = CatalogoId.toArray(new String[CatalogoId.size()]);

                    for (Clase_Procesador mostrarnucleos : procesadores){
                        CatalogoNucleos.add(mostrarnucleos.getNucleos());
                    }
                    nucleos = CatalogoNucleos.toArray(new String[CatalogoNucleos.size()]);
                    for (Clase_Procesador mostrarfe : procesadores){
                        CatalogoFrecuencia.add(mostrarfe.getFrecuencia());
                    }
                    frecuencia = CatalogoFrecuencia.toArray(new String[CatalogoFrecuencia.size()]);


                }else{
                    Toast.makeText(Pro.this,"Error 500",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Procesador> call, Throwable t) {
                Toast.makeText(Pro.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });







    }



}


