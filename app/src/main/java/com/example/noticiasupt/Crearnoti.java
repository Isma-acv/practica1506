package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Crearnoti extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crearnoti);

        ImageButton BtnEnviarNot = (ImageButton) findViewById(R.id.imageButtonEnviarMensaje);
        BtnEnviarNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText id = (EditText) findViewById(R.id.editTextTextNombreid);
                EditText titulo = (EditText) findViewById(R.id.editTextTextTitulo);
                EditText descripcion = (EditText) findViewById(R.id.editTextTextDescripcion);

                String mid = id.getText().toString();
                String mtitulo = titulo.getText().toString();
                String mdescripcion = descripcion.getText().toString();

                if(TextUtils.isEmpty(mid)){
                    id.setError("Ingres el id");
                    id.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(mtitulo)){
                    titulo.setError("Escriba un titulo");
                    titulo.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(mdescripcion)){
                    descripcion.setError("Escriba su descripcion");
                    descripcion.requestFocus();
                    return;
                }

                // -- PETICION PARA REGISTRAR USUARIOS

                ServicioPeticion service = Api.getApi(Crearnoti.this).create(ServicioPeticion.class);
                Call<Peticion_Cnotificacion> registrarCall = service.get_Cnotificacion(id.getText().toString(), titulo.getText().toString(), descripcion.getText().toString());
                registrarCall.enqueue(new Callback<Peticion_Cnotificacion>() {
                    @Override
                    public void onResponse(Call<Peticion_Cnotificacion> call, Response<Peticion_Cnotificacion> response) {
                        Peticion_Cnotificacion peticion = response.body();
                        if (response.body() == null){
                            Toast.makeText(Crearnoti.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (peticion.estado == "true"){
                            Toast.makeText(Crearnoti.this,"EXITO! Se envio la notifiacion",Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(Crearnoti.this, peticion.detalle,Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Cnotificacion> call, Throwable t) {
                        Toast.makeText(Crearnoti.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });//butoon Enviar notificacion


    }//Oncreate


}//Clase