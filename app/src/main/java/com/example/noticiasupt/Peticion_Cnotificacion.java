package com.example.noticiasupt;

public class Peticion_Cnotificacion {

    public String estado;
    public String detalle;
    public String usuarioId;
    public String titulo;
    public String descripcion;

    public Peticion_Cnotificacion(String usuarioId, String titulo, String descripcion){
        this.usuarioId = usuarioId;
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


}
