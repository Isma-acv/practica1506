package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    //-- VARIABLES QUE VOY A OCUPAR
    public String APITOKEN;
    public String NAMEUSUARIO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //-- VERIFICAR QUE EL TOKEN AUN EXISTA
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != ""){
            Intent exiToken= new Intent(MainActivity.this,Panel.class);
            startActivity(exiToken);
        }


        //-- CODIGO DEL BOTON PARA REALIZAR LA PETICION
        final Button btnIniciar = (Button) findViewById(R.id.buttonIniciarSesion);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText EdtUsuario  = (EditText) findViewById(R.id.editTextCorreo);
                EditText EdtPassword = (EditText) findViewById(R.id.editTextContrasena);
                String usuario = EdtUsuario.getText().toString();
                String password = EdtPassword.getText().toString();

                if(TextUtils.isEmpty(usuario)){
                    EdtUsuario.setError("Porfavor Ingrese su Usuario");
                    EdtUsuario.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    EdtPassword.setError("Ups! se le olvida lo mas Importante");
                    EdtPassword.requestFocus();
                    return;
                }

                //------------------------------- EMPIEZO A REALIZAR LA PETICION A LA API DE LOGIN

                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> iniciarCall = service.get_Login(EdtUsuario.getText().toString(), EdtPassword.getText().toString());
                iniciarCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();

                        if (response.body() == null){
                            Toast.makeText(MainActivity.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            NAMEUSUARIO = peticion.usuario;
                            guardarPreferencias();
                            Toast.makeText(MainActivity.this,"Bienvenido: "+NAMEUSUARIO,Toast.LENGTH_LONG).show();
                            Intent IraPanel = new Intent(MainActivity.this,Panel.class);
                            IraPanel.putExtra("username", NAMEUSUARIO);
                            startActivity(IraPanel);


                        }else {
                            Toast.makeText(MainActivity.this,peticion.detalle,Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
                    }
                }); // TERMINA PETICION



            }
        }); // TERMINA CODIGO DEL BOTON





    }// TERMINA ONCREATE


    //-- METODODO PARA QUE ME LLEVA REGISTRO
    public void Goregistro(View view){
        Intent IraRegistro = new Intent(MainActivity.this,Registro.class);
        startActivity(IraRegistro);
    }


    //-- METODO PARA GUARDAR EL TOKEN EN UN ARCHIVO
    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}
