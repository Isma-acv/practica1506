package com.example.noticiasupt;

import java.util.ArrayList;
import java.util.List;

public class Peticion_Noticia {

    public String estado;
    public List<ClaseDetalle> detalle;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<ClaseDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ClaseDetalle> detalle) {
        this.detalle = detalle;
    }



}
