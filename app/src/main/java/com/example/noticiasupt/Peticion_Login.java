package com.example.noticiasupt;

public class Peticion_Login {

    public String usuario;
    public String contrasena;
    public String token;
    public String detalle;
    public String estado;
    public String nameusuario;

    public Peticion_Login(String nameusuario, String contrasena){
        this.nameusuario = nameusuario;
        this.contrasena = contrasena;
    }


    public String getNameusuario() {
        return nameusuario;
    }

    public void setNameusuario(String nameusuario) {
        this.nameusuario = nameusuario;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }



    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }


}
