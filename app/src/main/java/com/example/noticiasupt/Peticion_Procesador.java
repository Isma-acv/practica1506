package com.example.noticiasupt;

import java.util.ArrayList;

public class Peticion_Procesador {

    public String estado;
    public String detalle;

    public ArrayList<Clase_Procesador> getProcesadores() {
        return procesadores;
    }

    public void setProcesadores(ArrayList<Clase_Procesador> procesadores) {
        this.procesadores = procesadores;
    }

    public ArrayList<Clase_Procesador> procesadores;


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }







}
