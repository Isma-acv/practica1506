package com.example.noticiasupt;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username")String correo, @Field("password")String password);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> get_Login(@Field("username")String nameusuario, @Field("password")String contrasena);


    @GET("api/todasNot")
    Call<Peticion_Noticia> get_Noticias();

    @POST("api/todosUsuarios")
    Call<Peticion_ListaUsuarios> get_Usuarios();

    @POST("api/informacionProcesadores")
    Call<Peticion_Procesador> get_Procesadores();

    @POST("api/detalleProcesador")
    Call<Peticion_DetalleProcesador> get_detalleprocesador();

    // Enviar notificacion
    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Peticion_Cnotificacion> get_Cnotificacion(@Field("usuarioId")String usuarioId, @Field("titulo")String titulo, @Field("descripcion")String descripcion);

}
